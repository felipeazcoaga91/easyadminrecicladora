<?php

namespace App\Repository;

use App\Entity\OrdenVenta;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrdenVenta|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrdenVenta|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrdenVenta[]    findAll()
 * @method OrdenVenta[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrdenVentaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrdenVenta::class);
    }

    // /**
    //  * @return OrdenVenta[] Returns an array of OrdenVenta objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrdenVenta
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
