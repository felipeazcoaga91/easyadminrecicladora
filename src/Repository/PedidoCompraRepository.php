<?php

namespace App\Repository;

use App\Entity\PedidoCompra;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PedidoCompra|null find($id, $lockMode = null, $lockVersion = null)
 * @method PedidoCompra|null findOneBy(array $criteria, array $orderBy = null)
 * @method PedidoCompra[]    findAll()
 * @method PedidoCompra[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PedidoCompraRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PedidoCompra::class);
    }

    // /**
    //  * @return PedidoCompra[] Returns an array of PedidoCompra objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PedidoCompra
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
