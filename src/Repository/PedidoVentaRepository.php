<?php

namespace App\Repository;

use App\Entity\PedidoVenta;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PedidoVenta|null find($id, $lockMode = null, $lockVersion = null)
 * @method PedidoVenta|null findOneBy(array $criteria, array $orderBy = null)
 * @method PedidoVenta[]    findAll()
 * @method PedidoVenta[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PedidoVentaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PedidoVenta::class);
    }

    // /**
    //  * @return PedidoVenta[] Returns an array of PedidoVenta objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PedidoVenta
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
