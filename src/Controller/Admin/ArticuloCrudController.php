<?php

namespace App\Controller\Admin;

use App\Entity\Articulo;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Require ROLE_ADMIN for *every* controller method in this class.
 *
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_PROD') or is_granted('ROLE_VENTA') or is_granted('ROLE_COMPRA') or is_granted('ROLE_RRHH')")
 */
class ArticuloCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Articulo::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            FormField::addPanel('Detalles', 'fa fa-list'),
            AssociationField::new('cargado_por', 'Cargado por')->setColumns('col-6'),
            AssociationField::new('categoria_art', 'Categoria')->setColumns('col-6'),
            TextField::new('nombre_art', 'Nombre')->setColumns('col-6'),
            TextField::new('descripcion', 'Descripción')->setColumns('col-6'),
            TextField::new('tipo_art', 'Tipo')->setColumns('col-4'),
            TextField::new('marca', 'Marca')->setColumns('col-4'),
            MoneyField::new('costo_unit', 'Costo Unitario (por kg)')->setCurrency('ARS')->setColumns('col-4'),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            
            ->setPageTitle('index', 'Articulos')
            ->setPageTitle('new', 'Nuevo Articulo')
            ->setPageTitle('detail', 'Detalles Articulo')
            ->setPageTitle('edit', 'Editar Articulo')
        ;
    }
}
