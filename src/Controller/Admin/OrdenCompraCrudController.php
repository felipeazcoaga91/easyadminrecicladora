<?php

namespace App\Controller\Admin;

use App\Entity\OrdenCompra;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Require ROLE_ADMIN for *every* controller method in this class.
 *
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_COMPRA')")
 */
class OrdenCompraCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return OrdenCompra::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            FormField::addPanel('Detalles', 'fa fa-list'),
            AssociationField::new('articulo', 'Articulo')->setColumns('col-4'),
            NumberField::new('cant', 'Cantidad')->setColumns('col-4'),
            MoneyField::new('subtotal', 'Subtotal')->setCurrency('ARS')->setColumns('col-4'),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            
            ->setPageTitle('index', 'Ordenes de Compra')
            ->setPageTitle('new', 'Nueva Orden')
            ->setPageTitle('detail', 'Detalles Orden')
            ->setPageTitle('edit', 'Editar Orden')
        ;
    }
}
