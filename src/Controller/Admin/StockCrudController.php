<?php

namespace App\Controller\Admin;

use App\Entity\Stock;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Require ROLE_ADMIN for *every* controller method in this class.
 *
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_PROD')")
 */
class StockCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Stock::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            FormField::addPanel('Detalles', 'fa fa-list'),
            AssociationField::new('articulo', 'Articulo')->setColumns('col-4'),
            NumberField::new('peso', 'Peso (en kg)')->setColumns('col-4'),
            NumberField::new('medidas', 'Medidas (en metros cuadrados)')->setColumns('col-4'),
            TextField::new('zona_alm', 'Zona de Almacenaje')->setColumns('col-4'),
            DateField::new('fec_alm', 'Fecha de almacenado')->setColumns('col-4'),
            NumberField::new('temp_prom', 'Temperatura Promedio (en grados centígrados)')->setColumns('col-4'),
            NumberField::new('cant', 'Cantidad (por kg)'),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            
            ->setPageTitle('index', 'Stocks')
            ->setPageTitle('new', 'Nuevo Stock')
            ->setPageTitle('detail', 'Detalles Stock')
            ->setPageTitle('edit', 'Editar Stock')
        ;
    }
}
