<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Require ROLE_ADMIN for *every* controller method in this class.
 *
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_RRHH')")
 */
class UserCrudController extends AbstractCrudController
{

    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            FormField::addPanel('Detalles', 'fa fa-list'),
            AssociationField::new('creado_por', 'Creado por')->setColumns('col-4')->hideOnIndex(),
            ChoiceField::new('roles', 'Rol')
            ->setChoices(fn () => [
                'Admin' => 'ROLE_ADMIN', 
                'Venta' => 'ROLE_VENTA',
                'Compra' => 'ROLE_COMPRA',
                'Producción' => 'ROLE_PROD',
                'Administración' => 'ROLE_RRHH'
            ])->allowMultipleChoices(),
            TextField::new('email', 'E-mail')->setColumns('col-4'),
            TextField::new('password', 'Contraseña')->setColumns('col-4')->hideOnIndex(),
            TextField::new('username', 'Nombre de Usuario')->setColumns('col-4'),
            NumberField::new('dni', 'DNI')->setColumns('col-4'),
            TextField::new('telefono', 'Telefono')->setColumns('col-6'),
            TextField::new('direccion', 'Direccion')->setColumns('col-6'),
            DateField::new('fec_nac', 'Fecha de Nacimiento')->setColumns('col-6'),
            DateField::new('fec_alta', 'Fecha de alta')->setColumns('col-6')->hideOnIndex(),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            
            ->setPageTitle('index', 'Usuarios')
            ->setPageTitle('new', 'Nuevo Usuario')
            ->setPageTitle('detail', 'Detalles Usuario')
            ->setPageTitle('edit', 'Editar Usuario')
        ;
    }
}
