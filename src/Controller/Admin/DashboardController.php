<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Usuario;
use App\Entity\User;
use App\Entity\Cliente;
use App\Entity\Proveedor;
use App\Entity\Articulo;
use App\Entity\Venta;
use App\Entity\Compra;
use App\Entity\Categoria;
use App\Entity\OrdenCompra;
use App\Entity\OrdenVenta;
use App\Entity\PedidoCompra;
use App\Entity\PedidoVenta;
use App\Entity\Rol;
use App\Entity\Stock;

use App\Controller\Admin\UsuarioCrudController;
use App\Controller\Admin\UserCrudController;
use App\Controller\Admin\ClienteCrudController;
use App\Controller\Admin\ProveedorCrudController;
use App\Controller\Admin\ArticuloCrudController;
use App\Controller\Admin\VentaCrudController;
use App\Controller\Admin\CompraCrudController;
use App\Controller\Admin\CategoriaCrudController;
use App\Controller\Admin\OrdenCrudController;
use App\Controller\Admin\PedidoCompraCrudController;
use App\Controller\Admin\PedidoVentaCrudController;
use App\Controller\Admin\RolCrudController;
use App\Controller\Admin\StockCrudController;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(AdminUrlGenerator::class);

        return $this->redirect($routeBuilder->setController(ArticuloCrudController::class)->generateUrl());
        return $this->redirect($routeBuilder->setController(ClienteCrudController::class)->generateUrl());
        return $this->redirect($routeBuilder->setController(ProveedorCrudController::class)->generateUrl());
        return $this->redirect($routeBuilder->setController(UserCrudController::class)->generateUrl());
        return $this->redirect($routeBuilder->setController(VentaCrudController::class)->generateUrl());
        return $this->redirect($routeBuilder->setController(CompraCrudController::class)->generateUrl());
        return $this->redirect($routeBuilder->setController(PedidoVentaCrudController::class)->generateUrl());
        return $this->redirect($routeBuilder->setController(PedidoCompraCrudController::class)->generateUrl());
        return $this->redirect($routeBuilder->setController(CategoriaCrudController::class)->generateUrl());
        return $this->redirect($routeBuilder->setController(StockCrudController::class)->generateUrl());
        return $this->redirect($routeBuilder->setController(OrdenCompraCrudController::class)->generateUrl());
        return $this->redirect($routeBuilder->setController(OrdenVentaCrudController::class)->generateUrl());
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Easy Admin Recicladora');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Menú Principal', 'fa fa-home');

        yield MenuItem::subMenu('Usuarios', 'fas fa-user-friends')->setSubItems([
            MenuItem::linkToCrud('Listado', 'fas fa-list', User::class),
        ]);
        yield MenuItem::subMenu('Ventas', 'fas fa-ticket-alt')->setSubItems([
            MenuItem::linkToCrud('Clientes', 'fas fa-user-friends', Cliente::class),
            MenuItem::linkToCrud('Ordenes de Venta', 'fab fa-servicestack', OrdenVenta::class),
            MenuItem::linkToCrud('Pedidos de Venta', 'fas fa-map-marker-alt', PedidoVenta::class),
            MenuItem::linkToCrud('Listado de Ventas', 'fas fa-dollar-sign', Venta::class),
        ]);
        yield MenuItem::subMenu('Compras', 'fas fa-ticket-alt')->setSubItems([
            MenuItem::linkToCrud('Proveedores', 'fas fa-user-friends', Proveedor::class),
            MenuItem::linkToCrud('Ordenes de Compra', 'fab fa-servicestack', OrdenCompra::class),
            MenuItem::linkToCrud('Pedidos de Compra', 'fas fa-map-marker-alt', PedidoCompra::class),
            MenuItem::linkToCrud('Listado de Compras', 'fas fa-dollar-sign', Compra::class),
        ]);
        yield MenuItem::subMenu('Articulos', 'fas fa-box-open')->setSubItems([
            MenuItem::linkToCrud('Listado de Articulos', 'fas fa-boxes', Articulo::class),
            MenuItem::linkToCrud('Categorias de Articulos', 'fas fa-list', Categoria::class),
            MenuItem::linkToCrud('Listado de Stocks', 'fas fa-truck-loading', Stock::class),
        ]);

    }
}
