<?php

namespace App\Controller\Admin;

use App\Entity\Cliente;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Require ROLE_ADMIN for *every* controller method in this class.
 *
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_VENTA')")
 */
class ClienteCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Cliente::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            FormField::addPanel('Detalles', 'fa fa-list'),
            AssociationField::new('creado_por', 'Creado por')->setColumns('col-4'),
            NumberField::new('dni', 'DNI')->setColumns('col-4'),
            TextField::new('email', 'E-mail')->setColumns('col-4'),
            TextField::new('nombre', 'Nombre')->setColumns('col-4'),
            TextField::new('telefono', 'Telefono')->setColumns('col-4'),
            TextField::new('direccion', 'Direccion')->setColumns('col-4'),
            TextField::new('descripcion', 'Descripcion')->setColumns('col-6'),
            DateField::new('fec_alta', 'Fecha de alta')->setColumns('col-6'),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            
            ->setPageTitle('index', 'Clientes')
            ->setPageTitle('new', 'Nuevo Cliente')
            ->setPageTitle('detail', 'Detalles Cliente')
            ->setPageTitle('edit', 'Editar Cliente')
        ;
    }
}
