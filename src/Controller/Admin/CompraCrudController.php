<?php

namespace App\Controller\Admin;

use App\Entity\Compra;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

 /**
  * Require ROLE_ADMIN for *every* controller method in this class.
  *
  * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_RRHH') or is_granted('ROLE_COMPRA')")
  */
class CompraCrudController extends AbstractCrudController
{

    public static function getEntityFqcn(): string
    {
        return Compra::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            FormField::addPanel('Detalles', 'fa fa-list'),
            IdField::new('id')->hideOnForm(),
            AssociationField::new('pedidoCompras', 'Pedidos de Compra')->setColumns('col-6')->hideOnIndex(),
            MoneyField::new('importe_total', 'Importe Total')->setCurrency('ARS')->setColumns('col-6'),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            
            ->setPageTitle('index', 'Compras')
            ->setPageTitle('new', 'Nueva Compra')
            ->setPageTitle('detail', 'Detalles Compra')
            ->setPageTitle('edit', 'Editar Compra')
        ;
    }
}
