<?php

namespace App\Controller\Admin;

use App\Entity\Venta;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Require ROLE_ADMIN for *every* controller method in this class.
 *
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_RRHH') or is_granted('ROLE_VENTA')")
 */
class VentaCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Venta::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            FormField::addPanel('Detalles', 'fa fa-list'),
            IdField::new('id')->hideOnForm(),
            AssociationField::new('pedidoVentas', 'Pedido de Venta')->setColumns('col-6')->hideOnIndex(),
            MoneyField::new('importe_total', 'Importe Total')->setCurrency('ARS')->setColumns('col-6'),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            
            ->setPageTitle('index', 'Ventas')
            ->setPageTitle('new', 'Nueva Venta')
            ->setPageTitle('detail', 'Detalles Venta')
            ->setPageTitle('edit', 'Editar Venta')
        ;
    }
}
