<?php

namespace App\Controller\Admin;

use App\Entity\PedidoCompra;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Require ROLE_ADMIN for *every* controller method in this class.
 *
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_COMPRA')")
 */
class PedidoCompraCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return PedidoCompra::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            FormField::addPanel('Detalles', 'fa fa-list'),
            IdField::new('id')->hideOnForm(),
            AssociationField::new('cargado_por', 'Cargado Por')->setColumns('col-4'),
            AssociationField::new('proveedor', 'Proveedor')->setColumns('col-4'),
            AssociationField::new('ordenCompras', 'Ordenes')->setColumns('col-4')->hideOnIndex(),
            MoneyField::new('importe', 'Importe')->setCurrency('ARS')->setColumns('col-4'),
            TextField::new('descripcion', 'Descripcion')->setColumns('col-4'),
            DateField::new('fec_pedido', 'Fecha de Pedido')->setColumns('col-4'),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            
            ->setPageTitle('index', 'Pedidos de Compra')
            ->setPageTitle('new', 'Nuevo Pedido')
            ->setPageTitle('detail', 'Detalles Pedido')
            ->setPageTitle('edit', 'Editar Pedido')
        ;
    }
}
