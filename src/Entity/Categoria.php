<?php

namespace App\Entity;

use App\Repository\CategoriaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategoriaRepository::class)
 */
class Categoria
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre_cat;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $creada_por;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombreCat(): ?string
    {
        return $this->nombre_cat;
    }

    public function setNombreCat(string $nombre_cat): self
    {
        $this->nombre_cat = $nombre_cat;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getCreadaPor(): ?User
    {
        return $this->creada_por;
    }

    public function setCreadaPor(?User $creada_por): self
    {
        $this->creada_por = $creada_por;

        return $this;
    }

    public function __toString()
    {
        return $this->nombre_cat;
    }
}
