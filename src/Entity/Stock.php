<?php

namespace App\Entity;

use App\Repository\StockRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StockRepository::class)
 */
class Stock
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $peso;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $zona_alm;

    /**
     * @ORM\Column(type="float")
     */
    private $medidas;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fec_alm;

    /**
     * @ORM\Column(type="float")
     */
    private $temp_prom;

    /**
     * @ORM\Column(type="integer")
     */
    private $cant;

    /**
     * @ORM\ManyToOne(targetEntity=Articulo::class, inversedBy="stocks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $articulo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPeso(): ?float
    {
        return $this->peso;
    }

    public function setPeso(float $peso): self
    {
        $this->peso = $peso;

        return $this;
    }

    public function getZonaAlm(): ?string
    {
        return $this->zona_alm;
    }

    public function setZonaAlm(string $zona_alm): self
    {
        $this->zona_alm = $zona_alm;

        return $this;
    }

    public function getMedidas(): ?float
    {
        return $this->medidas;
    }

    public function setMedidas(float $medidas): self
    {
        $this->medidas = $medidas;

        return $this;
    }

    public function getFecAlm(): ?\DateTimeInterface
    {
        return $this->fec_alm;
    }

    public function setFecAlm(\DateTimeInterface $fec_alm): self
    {
        $this->fec_alm = $fec_alm;

        return $this;
    }

    public function getTempProm(): ?float
    {
        return $this->temp_prom;
    }

    public function setTempProm(float $temp_prom): self
    {
        $this->temp_prom = $temp_prom;

        return $this;
    }

    public function getCant(): ?int
    {
        return $this->cant;
    }

    public function setCant(int $cant): self
    {
        $this->cant = $cant;

        return $this;
    }

    public function getArticulo(): ?Articulo
    {
        return $this->articulo;
    }

    public function setArticulo(?Articulo $articulo): self
    {
        $this->articulo = $articulo;

        return $this;
    }

}
