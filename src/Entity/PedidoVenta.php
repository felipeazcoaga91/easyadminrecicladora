<?php

namespace App\Entity;

use App\Repository\PedidoVentaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PedidoVentaRepository::class)
 */
class PedidoVenta
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $importe;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fec_pedido;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $cargado_por;

    /**
     * @ORM\ManyToOne(targetEntity=Cliente::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $cliente;

    /**
     * @ORM\ManyToOne(targetEntity=Venta::class, inversedBy="pedidoVentas")
     * @ORM\JoinColumn(name="venta_id", referencedColumnName="id",nullable=true)
     */
    private $venta;

    /**
     * @ORM\OneToMany(targetEntity=OrdenVenta::class, mappedBy="pedido_venta")
     */
    private $ordenVentas;

    public function __construct()
    {
        $this->ordenVentas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImporte(): ?float
    {
        return $this->importe;
    }

    public function setImporte(float $importe): self
    {
        $this->importe = $importe;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getFecPedido(): ?\DateTimeInterface
    {
        return $this->fec_pedido;
    }

    public function setFecPedido(\DateTimeInterface $fec_pedido): self
    {
        $this->fec_pedido = $fec_pedido;

        return $this;
    }

    public function getCargadoPor(): ?User
    {
        return $this->cargado_por;
    }

    public function setCargadoPor(?User $cargado_por): self
    {
        $this->cargado_por = $cargado_por;

        return $this;
    }

    public function getCliente(): ?Cliente
    {
        return $this->cliente;
    }

    public function setCliente(?Cliente $cliente): self
    {
        $this->cliente = $cliente;

        return $this;
    }

    public function __toString()
    {
        return (string) $this->id;
    }

    /**
     * @return Collection|OrdenVenta[]
     */
    public function getOrdenVentas(): Collection
    {
        return $this->ordenVentas;
    }

    public function addOrdenVenta(OrdenVenta $ordenVenta): self
    {
        if (!$this->ordenVentas->contains($ordenVenta)) {
            $this->ordenVentas[] = $ordenVenta;
            $ordenVenta->setPedidoVenta($this);
        }

        return $this;
    }

    public function removeOrdenVenta(OrdenVenta $ordenVenta): self
    {
        if ($this->ordenVentas->removeElement($ordenVenta)) {
            // set the owning side to null (unless already changed)
            if ($ordenVenta->getPedidoVenta() === $this) {
                $ordenVenta->setPedidoVenta(null);
            }
        }

        return $this;
    }

    public function getVenta(): ?Venta
    {
        return $this->venta;
    }

    public function setVenta(?Venta $venta): self
    {
        $this->venta = $venta;

        return $this;
    }

}
