<?php

namespace App\Entity;

use App\Repository\OrdenCompraRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrdenCompraRepository::class)
 */
class OrdenCompra
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $subtotal;

    /**
     * @ORM\Column(type="integer")
     */
    private $cant;

    /**
     * @ORM\ManyToOne(targetEntity=Articulo::class, inversedBy="ordenCompras")
     * @ORM\JoinColumn(nullable=false)
     */
    private $articulo;

    /**
     * @ORM\ManyToOne(targetEntity=PedidoCompra::class, inversedBy="ordenCompras")
     * @ORM\JoinColumn(name="pedido_compra_id", referencedColumnName="id",nullable=true)
     */
    private $pedido_compra;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubtotal(): ?float
    {
        return $this->subtotal;
    }

    public function setSubtotal(float $subtotal): self
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    public function getCant(): ?int
    {
        return $this->cant;
    }

    public function setCant(int $cant): self
    {
        $this->cant = $cant;

        return $this;
    }

    public function getArticulo(): ?Articulo
    {
        return $this->articulo;
    }

    public function setArticulo(?Articulo $articulo): self
    {
        $this->articulo = $articulo;

        return $this;
    }

    public function getPedidoCompra(): ?PedidoCompra
    {
        return $this->pedido_compra;
    }

    public function setPedidoCompra(?PedidoCompra $pedido_compra): self
    {
        $this->pedido_compra = $pedido_compra;

        return $this;
    }

    public function __toString()
    {
        return (string) $this->id;
    }

}
