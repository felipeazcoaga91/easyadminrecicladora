<?php

namespace App\Entity;

use App\Repository\ArticuloRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ArticuloRepository::class)
 */
class Articulo
{
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;    

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre_art;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tipo_art;

    /**
     * @ORM\Column(type="float")
     */
    private $costo_unit;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $marca;

    /**
     * @ORM\ManyToOne(targetEntity=Categoria::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $categoria_art;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $cargado_por;

    /**
     * @ORM\OneToMany(targetEntity=OrdenCompra::class, mappedBy="articulo")
     */
    private $ordenCompras;

    /**
     * @ORM\OneToMany(targetEntity=OrdenVenta::class, mappedBy="articulo")
     */
    private $ordenVentas;

    /**
     * @ORM\OneToMany(targetEntity=Stock::class, mappedBy="articulo")
     */
    private $stocks;

    public function __construct()
    {
        $this->stock = new ArrayCollection();
        $this->ordenCompras = new ArrayCollection();
        $this->ordenVentas = new ArrayCollection();
        $this->stocks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombreArt(): ?string
    {
        return $this->nombre_art;
    }

    public function setNombreArt(string $nombre_art): self
    {
        $this->nombre_art = $nombre_art;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getTipoArt(): ?string
    {
        return $this->tipo_art;
    }

    public function setTipoArt(string $tipo_art): self
    {
        $this->tipo_art = $tipo_art;

        return $this;
    }

    public function getCostoUnit(): ?float
    {
        return $this->costo_unit;
    }

    public function setCostoUnit(float $costo_unit): self
    {
        $this->costo_unit = $costo_unit;

        return $this;
    }

    public function getMarca(): ?string
    {
        return $this->marca;
    }

    public function setMarca(string $marca): self
    {
        $this->marca = $marca;

        return $this;
    }

    public function getCategoriaArt(): ?Categoria
    {
        return $this->categoria_art;
    }

    public function setCategoriaArt(?Categoria $categoria_art): self
    {
        $this->categoria_art = $categoria_art;

        return $this;
    }

    public function getCargadoPor(): ?User
    {
        return $this->cargado_por;
    }

    public function setCargadoPor(?User $cargado_por): self
    {
        $this->cargado_por = $cargado_por;

        return $this;
    }

    public function __toString()
    {
        return $this->nombre_art;
    }

    /**
     * @return Collection|OrdenCompra[]
     */
    public function getOrdenCompras(): Collection
    {
        return $this->ordenCompras;
    }

    public function addOrdenCompra(OrdenCompra $ordenCompra): self
    {
        if (!$this->ordenCompras->contains($ordenCompra)) {
            $this->ordenCompras[] = $ordenCompra;
            $ordenCompra->setArticulo($this);
        }

        return $this;
    }

    public function removeOrdenCompra(OrdenCompra $ordenCompra): self
    {
        if ($this->ordenCompras->removeElement($ordenCompra)) {
            // set the owning side to null (unless already changed)
            if ($ordenCompra->getArticulo() === $this) {
                $ordenCompra->setArticulo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|OrdenVenta[]
     */
    public function getOrdenVentas(): Collection
    {
        return $this->ordenVentas;
    }

    public function addOrdenVenta(OrdenVenta $ordenVenta): self
    {
        if (!$this->ordenVentas->contains($ordenVenta)) {
            $this->ordenVentas[] = $ordenVenta;
            $ordenVenta->setArticulo($this);
        }

        return $this;
    }

    public function removeOrdenVenta(OrdenVenta $ordenVenta): self
    {
        if ($this->ordenVentas->removeElement($ordenVenta)) {
            // set the owning side to null (unless already changed)
            if ($ordenVenta->getArticulo() === $this) {
                $ordenVenta->setArticulo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Stock[]
     */
    public function getStocks(): Collection
    {
        return $this->stocks;
    }

    public function addStock(Stock $stock): self
    {
        if (!$this->stocks->contains($stock)) {
            $this->stocks[] = $stock;
            $stock->setArticulo($this);
        }

        return $this;
    }

    public function removeStock(Stock $stock): self
    {
        if ($this->stocks->removeElement($stock)) {
            // set the owning side to null (unless already changed)
            if ($stock->getArticulo() === $this) {
                $stock->setArticulo(null);
            }
        }

        return $this;
    }
}
