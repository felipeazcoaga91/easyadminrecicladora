<?php

namespace App\Entity;

use App\Repository\PedidoCompraRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PedidoCompraRepository::class)
 */
class PedidoCompra
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $importe;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fec_pedido;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $cargado_por;

    /**
     * @ORM\ManyToOne(targetEntity=Proveedor::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $proveedor;

    /**
     * @ORM\ManyToOne(targetEntity=Compra::class, inversedBy="pedidoCompras")
     * @ORM\JoinColumn(name="compra_id", referencedColumnName="id",nullable=true)
     */
    private $compra;

    /**
     * @ORM\OneToMany(targetEntity=OrdenCompra::class, mappedBy="pedido_compra")
     */
    private $ordenCompras;

    public function __construct()
    {
        $this->ordenCompras = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImporte(): ?float
    {
        return $this->importe;
    }

    public function setImporte(float $importe): self
    {
        $this->importe = $importe;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getFecPedido(): ?\DateTimeInterface
    {
        return $this->fec_pedido;
    }

    public function setFecPedido(\DateTimeInterface $fec_pedido): self
    {
        $this->fec_pedido = $fec_pedido;

        return $this;
    }

    public function getCargadoPor(): ?User
    {
        return $this->cargado_por;
    }

    public function setCargadoPor(?User $cargado_por): self
    {
        $this->cargado_por = $cargado_por;

        return $this;
    }

    public function getProveedor(): ?Proveedor
    {
        return $this->proveedor;
    }

    public function setProveedor(?Proveedor $proveedor): self
    {
        $this->proveedor = $proveedor;

        return $this;
    }

    public function getCompra(): ?Compra
    {
        return $this->compra;
    }

    public function setCompra(?Compra $compra): self
    {
        $this->compra = $compra;

        return $this;
    }

    /**
     * @return Collection|OrdenCompra[]
     */
    public function getOrdenCompras(): Collection
    {
        return $this->ordenCompras;
    }

    public function addOrdenCompra(OrdenCompra $ordenCompra): self
    {
        if (!$this->ordenCompras->contains($ordenCompra)) {
            $this->ordenCompras[] = $ordenCompra;
            $ordenCompra->setPedidoCompra($this);
        }

        return $this;
    }

    public function removeOrdenCompra(OrdenCompra $ordenCompra): self
    {
        if ($this->ordenCompras->removeElement($ordenCompra)) {
            // set the owning side to null (unless already changed)
            if ($ordenCompra->getPedidoCompra() === $this) {
                $ordenCompra->setPedidoCompra(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return (string) $this->id;
    }

}
