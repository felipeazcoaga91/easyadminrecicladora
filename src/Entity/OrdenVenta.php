<?php

namespace App\Entity;

use App\Repository\OrdenVentaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrdenVentaRepository::class)
 */
class OrdenVenta
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $subtotal;

    /**
     * @ORM\Column(type="integer")
     */
    private $cant;

    /**
     * @ORM\ManyToOne(targetEntity=Articulo::class, inversedBy="ordenVentas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $articulo;

    /**
     * @ORM\ManyToOne(targetEntity=PedidoVenta::class, inversedBy="ordenVentas")
     * @ORM\JoinColumn(name="pedido_venta_id", referencedColumnName="id",nullable=true)
     */
    private $pedido_venta;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubtotal(): ?float
    {
        return $this->subtotal;
    }

    public function setSubtotal(float $subtotal): self
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    public function getCant(): ?int
    {
        return $this->cant;
    }

    public function setCant(int $cant): self
    {
        $this->cant = $cant;

        return $this;
    }

    public function getArticulo(): ?Articulo
    {
        return $this->articulo;
    }

    public function setArticulo(?Articulo $articulo): self
    {
        $this->articulo = $articulo;

        return $this;
    }

    public function getPedidoVenta(): ?PedidoVenta
    {
        return $this->pedido_venta;
    }

    public function setPedidoVenta(?PedidoVenta $pedido_venta): self
    {
        $this->pedido_venta = $pedido_venta;

        return $this;
    }

    public function __toString()
    {
        return (string) $this->id;
    }
}
