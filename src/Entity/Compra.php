<?php

namespace App\Entity;

use App\Repository\CompraRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CompraRepository::class)
 */
class Compra
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $importe_total;

    /**
     * @ORM\OneToMany(targetEntity=PedidoCompra::class, mappedBy="compra")
     */
    private $pedidoCompras;

    public function __construct()
    {
        $this->pedidoCompras = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImporteTotal(): ?float
    {
        return $this->importe_total;
    }

    public function setImporteTotal(float $importe_total): self
    {
        $this->importe_total = $importe_total;

        return $this;
    }

    /**
     * @return Collection|PedidoCompra[]
     */
    public function getPedidoCompras(): Collection
    {
        return $this->pedidoCompras;
    }

    public function addPedidoCompra(PedidoCompra $pedidoCompra): self
    {
        if (!$this->pedidoCompras->contains($pedidoCompra)) {
            $this->pedidoCompras[] = $pedidoCompra;
            $pedidoCompra->setCompra($this);
        }

        return $this;
    }

    public function removePedidoCompra(PedidoCompra $pedidoCompra): self
    {
        if ($this->pedidoCompras->removeElement($pedidoCompra)) {
            // set the owning side to null (unless already changed)
            if ($pedidoCompra->getCompra() === $this) {
                $pedidoCompra->setCompra(null);
            }
        }

        return $this;
    }
}
