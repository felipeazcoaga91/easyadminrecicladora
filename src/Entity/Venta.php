<?php

namespace App\Entity;

use App\Repository\VentaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VentaRepository::class)
 */
class Venta
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $importe_total;

    /**
     * @ORM\OneToMany(targetEntity=PedidoVenta::class, mappedBy="venta")
     */
    private $pedidoVentas;

    public function __construct()
    {
        $this->pedidoVentas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImporteTotal(): ?float
    {
        return $this->importe_total;
    }

    public function setImporteTotal(float $importe_total): self
    {
        $this->importe_total = $importe_total;

        return $this;
    }

    /**
     * @return Collection|PedidoVenta[]
     */
    public function getPedidoVentas(): Collection
    {
        return $this->pedidoVentas;
    }

    public function addPedidoVenta(PedidoVenta $pedidoVenta): self
    {
        if (!$this->pedidoVentas->contains($pedidoVenta)) {
            $this->pedidoVentas[] = $pedidoVenta;
            $pedidoVenta->setVenta($this);
        }

        return $this;
    }

    public function removePedidoVenta(PedidoVenta $pedidoVenta): self
    {
        if ($this->pedidoVentas->removeElement($pedidoVenta)) {
            // set the owning side to null (unless already changed)
            if ($pedidoVenta->getVenta() === $this) {
                $pedidoVenta->setVenta(null);
            }
        }

        return $this;
    }

}
