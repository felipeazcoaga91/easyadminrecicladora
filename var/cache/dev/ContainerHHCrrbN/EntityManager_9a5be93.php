<?php

namespace ContainerHHCrrbN;
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'persistence'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'Persistence'.\DIRECTORY_SEPARATOR.'ObjectManager.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder0d62b = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializerc4f4b = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiesb8658 = [
        
    ];

    public function getConnection()
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'getConnection', array(), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'getMetadataFactory', array(), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'getExpressionBuilder', array(), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'beginTransaction', array(), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->beginTransaction();
    }

    public function getCache()
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'getCache', array(), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->getCache();
    }

    public function transactional($func)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'transactional', array('func' => $func), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'wrapInTransaction', array('func' => $func), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'commit', array(), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->commit();
    }

    public function rollback()
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'rollback', array(), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'getClassMetadata', array('className' => $className), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'createQuery', array('dql' => $dql), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'createNamedQuery', array('name' => $name), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'createQueryBuilder', array(), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'flush', array('entity' => $entity), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'clear', array('entityName' => $entityName), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->clear($entityName);
    }

    public function close()
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'close', array(), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->close();
    }

    public function persist($entity)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'persist', array('entity' => $entity), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'remove', array('entity' => $entity), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'refresh', array('entity' => $entity), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'detach', array('entity' => $entity), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'merge', array('entity' => $entity), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'getRepository', array('entityName' => $entityName), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'contains', array('entity' => $entity), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'getEventManager', array(), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'getConfiguration', array(), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'isOpen', array(), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'getUnitOfWork', array(), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'getProxyFactory', array(), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'initializeObject', array('obj' => $obj), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'getFilters', array(), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'isFiltersStateClean', array(), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'hasFilters', array(), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return $this->valueHolder0d62b->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializerc4f4b = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder0d62b) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder0d62b = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder0d62b->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, '__get', ['name' => $name], $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        if (isset(self::$publicPropertiesb8658[$name])) {
            return $this->valueHolder0d62b->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder0d62b;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder0d62b;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder0d62b;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder0d62b;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, '__isset', array('name' => $name), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder0d62b;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder0d62b;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, '__unset', array('name' => $name), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder0d62b;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder0d62b;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, '__clone', array(), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        $this->valueHolder0d62b = clone $this->valueHolder0d62b;
    }

    public function __sleep()
    {
        $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, '__sleep', array(), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;

        return array('valueHolder0d62b');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializerc4f4b = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializerc4f4b;
    }

    public function initializeProxy() : bool
    {
        return $this->initializerc4f4b && ($this->initializerc4f4b->__invoke($valueHolder0d62b, $this, 'initializeProxy', array(), $this->initializerc4f4b) || 1) && $this->valueHolder0d62b = $valueHolder0d62b;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder0d62b;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder0d62b;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
