<?php

namespace Proxies\__CG__\App\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Articulo extends \App\Entity\Articulo implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array<string, null> properties to be lazy loaded, indexed by property name
     */
    public static $lazyPropertiesNames = array (
);

    /**
     * @var array<string, mixed> default values of properties to be lazy loaded, with keys being the property names
     *
     * @see \Doctrine\Common\Proxy\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array (
);



    public function __construct(?\Closure $initializer = null, ?\Closure $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'App\\Entity\\Articulo' . "\0" . 'id', '' . "\0" . 'App\\Entity\\Articulo' . "\0" . 'nombre_art', '' . "\0" . 'App\\Entity\\Articulo' . "\0" . 'descripcion', '' . "\0" . 'App\\Entity\\Articulo' . "\0" . 'tipo_art', '' . "\0" . 'App\\Entity\\Articulo' . "\0" . 'costo_unit', '' . "\0" . 'App\\Entity\\Articulo' . "\0" . 'marca', '' . "\0" . 'App\\Entity\\Articulo' . "\0" . 'categoria_art', '' . "\0" . 'App\\Entity\\Articulo' . "\0" . 'cargado_por', '' . "\0" . 'App\\Entity\\Articulo' . "\0" . 'ordenCompras', '' . "\0" . 'App\\Entity\\Articulo' . "\0" . 'ordenVentas', '' . "\0" . 'App\\Entity\\Articulo' . "\0" . 'stocks'];
        }

        return ['__isInitialized__', '' . "\0" . 'App\\Entity\\Articulo' . "\0" . 'id', '' . "\0" . 'App\\Entity\\Articulo' . "\0" . 'nombre_art', '' . "\0" . 'App\\Entity\\Articulo' . "\0" . 'descripcion', '' . "\0" . 'App\\Entity\\Articulo' . "\0" . 'tipo_art', '' . "\0" . 'App\\Entity\\Articulo' . "\0" . 'costo_unit', '' . "\0" . 'App\\Entity\\Articulo' . "\0" . 'marca', '' . "\0" . 'App\\Entity\\Articulo' . "\0" . 'categoria_art', '' . "\0" . 'App\\Entity\\Articulo' . "\0" . 'cargado_por', '' . "\0" . 'App\\Entity\\Articulo' . "\0" . 'ordenCompras', '' . "\0" . 'App\\Entity\\Articulo' . "\0" . 'ordenVentas', '' . "\0" . 'App\\Entity\\Articulo' . "\0" . 'stocks'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Articulo $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy::$lazyPropertiesDefaults as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @deprecated no longer in use - generated code now relies on internal components rather than generated public API
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId(): ?int
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function getNombreArt(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNombreArt', []);

        return parent::getNombreArt();
    }

    /**
     * {@inheritDoc}
     */
    public function setNombreArt(string $nombre_art): \App\Entity\Articulo
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNombreArt', [$nombre_art]);

        return parent::setNombreArt($nombre_art);
    }

    /**
     * {@inheritDoc}
     */
    public function getDescripcion(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDescripcion', []);

        return parent::getDescripcion();
    }

    /**
     * {@inheritDoc}
     */
    public function setDescripcion(string $descripcion): \App\Entity\Articulo
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDescripcion', [$descripcion]);

        return parent::setDescripcion($descripcion);
    }

    /**
     * {@inheritDoc}
     */
    public function getTipoArt(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTipoArt', []);

        return parent::getTipoArt();
    }

    /**
     * {@inheritDoc}
     */
    public function setTipoArt(string $tipo_art): \App\Entity\Articulo
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTipoArt', [$tipo_art]);

        return parent::setTipoArt($tipo_art);
    }

    /**
     * {@inheritDoc}
     */
    public function getCostoUnit(): ?float
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCostoUnit', []);

        return parent::getCostoUnit();
    }

    /**
     * {@inheritDoc}
     */
    public function setCostoUnit(float $costo_unit): \App\Entity\Articulo
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCostoUnit', [$costo_unit]);

        return parent::setCostoUnit($costo_unit);
    }

    /**
     * {@inheritDoc}
     */
    public function getMarca(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMarca', []);

        return parent::getMarca();
    }

    /**
     * {@inheritDoc}
     */
    public function setMarca(string $marca): \App\Entity\Articulo
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMarca', [$marca]);

        return parent::setMarca($marca);
    }

    /**
     * {@inheritDoc}
     */
    public function getCategoriaArt(): ?\App\Entity\Categoria
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCategoriaArt', []);

        return parent::getCategoriaArt();
    }

    /**
     * {@inheritDoc}
     */
    public function setCategoriaArt(?\App\Entity\Categoria $categoria_art): \App\Entity\Articulo
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCategoriaArt', [$categoria_art]);

        return parent::setCategoriaArt($categoria_art);
    }

    /**
     * {@inheritDoc}
     */
    public function getCargadoPor(): ?\App\Entity\User
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCargadoPor', []);

        return parent::getCargadoPor();
    }

    /**
     * {@inheritDoc}
     */
    public function setCargadoPor(?\App\Entity\User $cargado_por): \App\Entity\Articulo
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCargadoPor', [$cargado_por]);

        return parent::setCargadoPor($cargado_por);
    }

    /**
     * {@inheritDoc}
     */
    public function __toString()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, '__toString', []);

        return parent::__toString();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrdenCompras(): \Doctrine\Common\Collections\Collection
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOrdenCompras', []);

        return parent::getOrdenCompras();
    }

    /**
     * {@inheritDoc}
     */
    public function addOrdenCompra(\App\Entity\OrdenCompra $ordenCompra): \App\Entity\Articulo
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addOrdenCompra', [$ordenCompra]);

        return parent::addOrdenCompra($ordenCompra);
    }

    /**
     * {@inheritDoc}
     */
    public function removeOrdenCompra(\App\Entity\OrdenCompra $ordenCompra): \App\Entity\Articulo
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeOrdenCompra', [$ordenCompra]);

        return parent::removeOrdenCompra($ordenCompra);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrdenVentas(): \Doctrine\Common\Collections\Collection
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOrdenVentas', []);

        return parent::getOrdenVentas();
    }

    /**
     * {@inheritDoc}
     */
    public function addOrdenVenta(\App\Entity\OrdenVenta $ordenVenta): \App\Entity\Articulo
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addOrdenVenta', [$ordenVenta]);

        return parent::addOrdenVenta($ordenVenta);
    }

    /**
     * {@inheritDoc}
     */
    public function removeOrdenVenta(\App\Entity\OrdenVenta $ordenVenta): \App\Entity\Articulo
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeOrdenVenta', [$ordenVenta]);

        return parent::removeOrdenVenta($ordenVenta);
    }

    /**
     * {@inheritDoc}
     */
    public function getStocks(): \Doctrine\Common\Collections\Collection
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStocks', []);

        return parent::getStocks();
    }

    /**
     * {@inheritDoc}
     */
    public function addStock(\App\Entity\Stock $stock): \App\Entity\Articulo
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addStock', [$stock]);

        return parent::addStock($stock);
    }

    /**
     * {@inheritDoc}
     */
    public function removeStock(\App\Entity\Stock $stock): \App\Entity\Articulo
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeStock', [$stock]);

        return parent::removeStock($stock);
    }

}
